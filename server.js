///// import the modules you need /////
var express = require("express");
var http = require("http");
var mysql = require("mysql");
var app = express();
var bodyParser = require("body-parser");
/////////////////////////////////////////


///// parse all form data/////
app.use(bodyParser.urlencoded({ extended:true }));

///////////////////////////////

////// code for formatting date ////////////
var dateFormat = require("dateformat");
var now = new Date();
////////////////////////////////////////////

///////////// using the view engine ///////
app.set("view engine", "ejs");
///////////////////////////////////////////

///////// importing all JS and CSS file ////////
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/tether/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
////////////////////////////////////////////////

///////////////// creating a database with mysql //////////
const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'mydb'
}); 
///////////////////////////////////////////////////////////

///////////Global site title and URL /////////////////////
const siteTitle = "Simple CRUD Application";
const baseURL = "http://localhost:4000";
/////////////////////////////////////////////////////////

////////// when page loads /////////////////////////////
app.get("/", function(req, res) {

    //////get the event list /////

    res.render("pages/index", {
        siteTitle: siteTitle,
        pageTitle: "Event list",
        items: ""
    });

    // con.query("SELECT * FROM e_event ORDER BY e_start_date DESC", function(err, result) {
    //     res.render('pages/index', {
    //         siteTitle: siteTitle,
    //         pageTitle: "Event list",
    //         items: result
    //     });
    // });
});

///////////////////connect to the server/////////////
var server = app.listen(4000, function() {
    console.log("server has started on 4000");
});
////////////////////////////////////////////////////